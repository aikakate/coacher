package ru.bsu.coacher.eb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ru.bsu.coacher.entities.EnumType;
import ru.bsu.coacher.entities.EnumValue;

@Stateless
public class EnumTypeManager {
    @PersistenceContext
    private EntityManager em;
    
    public List<EnumType> findAllEnumTypes() {
        return em.createQuery("select e from EnumType e", EnumType.class).getResultList();
    }
    
    public EnumType create(String name, List<String> values, List<String> comments) {
        if (values == null || values.isEmpty())
            throw new IllegalArgumentException("values cannot be null");
        
        EnumType enumType = new EnumType();
        enumType.setName(name);
        for (int i = 0; i < values.size(); ++i) {
            EnumValue enumValue = new EnumValue();
            if (comments != null && comments.size() - 1 >= i)
                enumValue.setDescription(comments.get(i));
            enumValue.setType(enumType);
            enumValue.setName(values.get(i));
            
            enumType.getValues().add(enumValue);
        }
        em.persist(enumType);
        return enumType;
    }
    
    public EnumType find(long enumTypeId) {
        return em.find(EnumType.class, enumTypeId);
    }
    
    public EnumType findByCode(int code) {
        return em.createQuery(
        "select et from EnumType et where et.code = :code", EnumType.class)
        .setParameter("code", code)
        .getSingleResult();
    }
    
    public void delete(long enumTypeId) {
        em.createQuery("delete from EnumType e where e.id = :eid")
                .setParameter("eid", enumTypeId)
                .executeUpdate();
    }
    
    public void update(long enumTypeId, String name) {
        EnumType enumType = em.find(EnumType.class, enumTypeId);
        enumType.setName(name);
    }
}
