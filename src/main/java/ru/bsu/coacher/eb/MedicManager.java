package ru.bsu.coacher.eb;

import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ru.bsu.coacher.entities.Medic;
import ru.bsu.coacher.entities.Role;
import ru.bsu.coacher.shared.DataMapper;
import ru.bsu.coacher.shared.DefaultSpecialistData;
import ru.bsu.coacher.shared.SpecialistData;
import ru.bsu.coacher.shared.AccountData;

@Stateless
public class MedicManager {
    private static Logger logger = Logger.getLogger(MedicManager.class.getName());
    
    @EJB
    private AccountManager accMgr;
    
    @PersistenceContext
    private EntityManager em;
    
    public List<Medic> findAllMedics() {
        return em.createQuery("select m from Medic m", Medic.class).getResultList();
    }
    
    public Medic findMedic(long medicId) {
        return em.find(Medic.class, medicId);
    }
    
    public void updateMedicData(long medicId, DefaultSpecialistData data) {
        Medic medic = em.find(Medic.class, medicId);
        DataMapper.map((SpecialistData)data, medic);
        em.persist(medic);
    }
    
    public Medic createMedic(String email, String password, DefaultSpecialistData data) {
        Medic medic = new Medic();
        DataMapper.map((SpecialistData)data, medic);
        medic.setEmail(email);
        medic.setPasswordHash(password);
        medic.setApprovedEmail(true);
        medic.getRoles().add(em.find(Role.class, "medic"));
        medic.getRoles().add(em.find(Role.class, "master"));
        em.persist(medic);
        return medic;
    }
}
