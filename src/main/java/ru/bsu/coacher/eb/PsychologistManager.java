package ru.bsu.coacher.eb;

import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ru.bsu.coacher.entities.Psychologist;
import ru.bsu.coacher.entities.Role;
import ru.bsu.coacher.shared.DataMapper;
import ru.bsu.coacher.shared.DefaultSpecialistData;
import ru.bsu.coacher.shared.SpecialistData;
import ru.bsu.coacher.shared.AccountData;
import ru.bsu.coacher.shared.PasswordHasher;

@Stateless
public class PsychologistManager {
    private static Logger logger = Logger.getLogger(PsychologistManager.class.getName());
    
    @PersistenceContext
    private EntityManager em;
    
    @EJB
    private AccountManager accMgr;
    
    public List<Psychologist> findAllPsychologists() {
        return em.createQuery("select p from Psychologist p", Psychologist.class).getResultList();
    }
    
    public Psychologist findPsychologist(long psychId) {
        return em.find(Psychologist.class, psychId);
    }
    
    public void updatePsychologistData(long psychId, DefaultSpecialistData data) {
        Psychologist psycho = em.find(Psychologist.class, psychId);
        DataMapper.map((SpecialistData)data, psycho);
        em.persist(psycho);
    }
    
    public Psychologist createPsychologist(String email, String password, DefaultSpecialistData data) {
        Psychologist psycho = new Psychologist();
        DataMapper.map((SpecialistData)data, psycho);
        psycho.setEmail(email);
        psycho.setPasswordHash(PasswordHasher.hash(password));
        psycho.setApprovedEmail(true);
        psycho.getRoles().add(em.find(Role.class, "psychologist"));
        psycho.getRoles().add(em.find(Role.class, "master"));
        em.persist(psycho);
        return psycho;
    }
}
