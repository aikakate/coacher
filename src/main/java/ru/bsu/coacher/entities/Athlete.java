
package ru.bsu.coacher.entities;

import ru.bsu.coacher.shared.SportDegree;
import ru.bsu.coacher.shared.LivingPlace;
import ru.bsu.coacher.shared.FamilyStatus;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import ru.bsu.coacher.entities.records.RecordValue;
import ru.bsu.coacher.shared.AthleteData;

@Entity
public class Athlete extends Account implements AthleteData {
    private String birthPlace;
    private String nationality;
    private String religion;
    private String studyPlace;
    private float height;
    private float weight;
    private String sport;
    @Enumerated(javax.persistence.EnumType.STRING)
    private SportDegree sportDegree;
    private int parents;
    @Enumerated(javax.persistence.EnumType.STRING)
    private LivingPlace livingPlace;
    private String livingAddress;
    @Enumerated(javax.persistence.EnumType.STRING)
    private FamilyStatus familyStatus;
    private int children;
    private String diseases;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "athlete")
    private final Collection<CompetitionResult> competitionResults = new ArrayList<>();
    
    @OneToMany(cascade = CascadeType.ALL)
    private final List<RecordValue> records = new ArrayList<>();
    
    @ManyToMany(mappedBy = "attachedAthletes")
    private final List<Master> masters = new ArrayList<>();

    public List<Master> getMasters() {
        return masters;
    }
    
    public Coach getCoach() {
        Coach coach = null;
        for (Master master : masters) {
            if (master instanceof Coach) {
                coach = (Coach)master;
                break;
            }
        }
        return coach;
    }
    
    public void setCoach(Coach coach) {
        Coach existingCoach = getCoach();
        if (existingCoach != null)
            if (!masters.remove(existingCoach))
                throw new IllegalStateException();
        masters.add(coach);
    }

    @Override
    public String getBirthPlace() {
        return birthPlace;
    }

    @Override
    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    @Override
    public String getNationality() {
        return nationality;
    }

    @Override
    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Override
    public String getReligion() {
        return religion;
    }

    @Override
    public void setReligion(String religion) {
        this.religion = religion;
    }

    @Override
    public String getStudyPlace() {
        return studyPlace;
    }

    @Override
    public void setStudyPlace(String studyPlace) {
        this.studyPlace = studyPlace;
    }

    @Override
    public float getHeight() {
        return height;
    }

    @Override
    public void setHeight(float height) {
        this.height = height;
    }

    @Override
    public float getWeight() {
        return weight;
    }

    @Override
    public void setWeight(float weight) {
        this.weight = weight;
    }

    @Override
    public String getSport() {
        return sport;
    }

    @Override
    public void setSport(String sport) {
        this.sport = sport;
    }

    @Override
    public SportDegree getSportDegree() {
        return sportDegree;
    }

    @Override
    public void setSportDegree(SportDegree sportDegree) {
        this.sportDegree = sportDegree;
    }

    @Override
    public int getParents() {
        return parents;
    }

    @Override
    public void setParents(int parents) {
        this.parents = parents;
    }

    @Override
    public LivingPlace getLivingPlace() {
        return livingPlace;
    }

    @Override
    public void setLivingPlace(LivingPlace livingPlace) {
        this.livingPlace = livingPlace;
    }

    @Override
    public String getLivingAddress() {
        return livingAddress;
    }

    @Override
    public void setLivingAddress(String livingAddress) {
        this.livingAddress = livingAddress;
    }

    @Override
    public FamilyStatus getFamilyStatus() {
        return familyStatus;
    }

    @Override
    public void setFamilyStatus(FamilyStatus familyStatus) {
        this.familyStatus = familyStatus;
    }

    @Override
    public int getChildren() {
        return children;
    }

    @Override
    public void setChildren(int children) {
        this.children = children;
    }

    @Override
    public String getDiseases() {
        return diseases;
    }

    @Override
    public void setDiseases(String diseases) {
        this.diseases = diseases;
    }
    
    public List<RecordValue> getRecords() {
        return records;
    }

    public Collection<CompetitionResult> getCompetitionResults() {
        return competitionResults;
    }
}
