package ru.bsu.coacher.entities;

import ru.bsu.coacher.shared.CoachQualification;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import ru.bsu.coacher.shared.CoachData;

@Entity
public class Coach extends Master implements CoachData {
    
    @Enumerated(javax.persistence.EnumType.STRING)
    private CoachQualification qualification;

    public CoachQualification getQualification() {
        return qualification;
    }

    public void setQualification(CoachQualification qualification) {
        this.qualification = qualification;
    }
}
