package ru.bsu.coacher.entities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public abstract class Master extends Account {
    @ManyToMany
    @JoinTable(name = "master_athletes",
            joinColumns = {
                @JoinColumn(name = "master_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                @JoinColumn(name = "athlete_id", nullable = false, updatable = false)
            })
    private final List<Athlete> attachedAthletes = new ArrayList<>();
    
    @OneToMany(mappedBy = "master", cascade = CascadeType.ALL)
    private final Collection<AthleteRequest> requests = new ArrayList<>();

    public List<Athlete> getAttachedAthletes() {
        return attachedAthletes;
    }

    public Collection<AthleteRequest> getRequests() {
        return requests;
    }
}
