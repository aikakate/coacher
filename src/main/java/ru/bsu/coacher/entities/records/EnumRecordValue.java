package ru.bsu.coacher.entities.records;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import ru.bsu.coacher.entities.EnumValue;
import ru.bsu.coacher.shared.RecordValueType;

@Entity
public class EnumRecordValue extends RecordValue {
    @ManyToOne
    private EnumValue value;

    public EnumValue getValue() {
        return value;
    }

    public void setValue(EnumValue value) {
        this.value = value;
    }

    @Override
    public RecordValueType getValueType() {
        return RecordValueType.Enum;
    }
}
