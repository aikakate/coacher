package ru.bsu.coacher.entities.records;

import ru.bsu.coacher.shared.RecordSourceType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import ru.bsu.coacher.shared.RecordPeriodicity;

@Entity
public class RecordGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer code;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date added;
    
    private String name;
    
    private String comment;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "group",
            fetch = FetchType.EAGER)
    private final List<Record> records = new ArrayList<>();
    
    @Enumerated(javax.persistence.EnumType.STRING)
    private RecordSourceType sourceType;
    
    @Enumerated(javax.persistence.EnumType.STRING)
    private RecordPeriodicity periodicity;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public RecordPeriodicity getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(RecordPeriodicity periodicity) {
        this.periodicity = periodicity;
    }

    public RecordSourceType getSourceType() {
        return sourceType;
    }

    public void setSourceType(RecordSourceType sourceType) {
        this.sourceType = sourceType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getAdded() {
        return added;
    }

    public void setAdded(Date added) {
        this.added = added;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Record> getRecords() {
        return records;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RecordGroup)) {
            return false;
        }
        RecordGroup other = (RecordGroup) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.bsu.coacher.RecordGroup[ id=" + id + " ]";
    }

}
