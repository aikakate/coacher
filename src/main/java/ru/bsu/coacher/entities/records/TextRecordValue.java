package ru.bsu.coacher.entities.records;

import javax.persistence.Entity;
import ru.bsu.coacher.shared.RecordValueType;

@Entity
public class TextRecordValue extends RecordValue  {
    
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public RecordValueType getValueType() {
        return RecordValueType.Text;
    }
}
