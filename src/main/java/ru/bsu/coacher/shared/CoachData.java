package ru.bsu.coacher.shared;

public interface CoachData {
    CoachQualification getQualification();
    void setQualification(CoachQualification qualification);
}
