package ru.bsu.coacher.shared;

public enum CoachQualification {
    First("Первая"),
    Second("Вторая"),
    Highest("Высшая");
    private final String caption;

    public String getCaption() {
        return caption;
    }
    
    private CoachQualification(String caption) {
        this.caption = caption;
    }
}
