
package ru.bsu.coacher.shared;

public class DataMapper {
    public static void map(AccountData from, AccountData to) {
        to.setGender(from.getGender());
        to.setBirthDate(from.getBirthDate());
        to.setWorkplace(from.getWorkplace());
        to.setOccupation(from.getOccupation());
        to.setPhone(from.getPhone());
        to.setLastName(from.getLastName());
        to.setFirstName(from.getFirstName());
        to.setMiddleName(from.getMiddleName());
    }
    
    public static void map(AthleteData from, AthleteData to) {
        to.setBirthPlace(from.getBirthPlace());
        to.setNationality(from.getNationality());
        to.setReligion(from.getReligion());
        to.setStudyPlace(from.getStudyPlace());
        to.setHeight(from.getHeight());
        to.setWeight(from.getWeight());
        to.setSport(from.getSport());
        to.setSportDegree(from.getSportDegree());
        to.setParents(from.getParents());
        to.setLivingPlace(from.getLivingPlace());
        to.setLivingAddress(from.getLivingAddress());
        to.setFamilyStatus(from.getFamilyStatus());
        to.setChildren(from.getChildren());
        to.setDiseases(from.getDiseases());
    }
    
    public static void map(CoachData from, CoachData to) {
        to.setQualification(from.getQualification());
    }
    
    public static void map(SpecialistData from, SpecialistData to) {
        to.setActivityType(from.getActivityType());
        to.setHighSpeciality(from.getHighSpeciality());
        to.setAcademicDegree(from.getAcademicDegree());
        to.setAcademicSpeciality(from.getAcademicSpeciality());
        to.setAcademicTitle(from.getAcademicTitle());
    }
}
