package ru.bsu.coacher.shared;

public class DefaultCoachData implements CoachData {

    private CoachQualification qualification;
    
    @Override
    public CoachQualification getQualification() {
        return qualification;
    }

    @Override
    public void setQualification(CoachQualification qualification) {
        this.qualification = qualification;
    }
    
    public CoachQualification[] getAllQualifications() {
        return qualification.values();
    }
}
