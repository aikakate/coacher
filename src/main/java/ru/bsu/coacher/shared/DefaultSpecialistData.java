package ru.bsu.coacher.shared;

public class DefaultSpecialistData
        implements SpecialistData{
    private String activityType;
    private String highSpeciality;
    private String academicDegree;
    private String academicSpeciality;
    private String academicTitle;

    @Override
    public String getActivityType() {
        return activityType;
    }

    @Override
    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    @Override
    public String getHighSpeciality() {
        return highSpeciality;
    }

    @Override
    public void setHighSpeciality(String highSpeciality) {
        this.highSpeciality = highSpeciality;
    }

    @Override
    public String getAcademicDegree() {
        return academicDegree;
    }

    @Override
    public void setAcademicDegree(String academicDegree) {
        this.academicDegree = academicDegree;
    }

    @Override
    public String getAcademicSpeciality() {
        return academicSpeciality;
    }

    @Override
    public void setAcademicSpeciality(String academicSpeciality) {
        this.academicSpeciality = academicSpeciality;
    }

    @Override
    public String getAcademicTitle() {
        return academicTitle;
    }

    @Override
    public void setAcademicTitle(String academicTitle) {
        this.academicTitle = academicTitle;
    }
    
}
