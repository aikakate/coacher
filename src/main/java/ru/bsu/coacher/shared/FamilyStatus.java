package ru.bsu.coacher.shared;

public enum FamilyStatus {
    Single("Холост/не замужем"),
    Married("Женат/замужем");
    
    private final String caption;

    public String getCaption() {
        return caption;
    }
    
    private FamilyStatus(String caption) {
        this.caption = caption;
    }
}
