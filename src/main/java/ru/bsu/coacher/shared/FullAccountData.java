package ru.bsu.coacher.shared;

import java.util.Date;

public interface FullAccountData extends AccountData {
    String getEmail();
    void setEmail(String email);
    String getPassword();
    void setPassword(String password);
    @Override
    String getLastName();
    @Override
    void setLastName(String lastName);
    @Override
    String getFirstName();
    @Override
    void setFirstName(String firstName);
    @Override
    String getMiddleName();
    @Override
    void setMiddleName(String middleName);
    @Override
    Gender getGender();
    @Override
    void setGender(Gender gender);
    @Override
    Date getBirthDate();
    @Override
    void setBirthDate(Date birthDate);
    @Override
    String getWorkplace();
    @Override
    void setWorkplace(String workplace);
    @Override
    String getOccupation();
    @Override
    void setOccupation(String occupation);
    @Override
    String getPhone();
    @Override
    void setPhone(String phone);
}
