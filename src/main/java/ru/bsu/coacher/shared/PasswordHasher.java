package ru.bsu.coacher.shared;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Base64;

public class PasswordHasher {

    public static String hash(String inputString) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] outputBytes = digest.digest(inputString.getBytes(StandardCharsets.UTF_8));
            Base64.Encoder base64encoder = Base64.getEncoder();
            return base64encoder.encodeToString(outputBytes);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
