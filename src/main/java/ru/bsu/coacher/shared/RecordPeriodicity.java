package ru.bsu.coacher.shared;

public enum RecordPeriodicity {
    None("Нет"),
    Day("Каждый день"),
    WorkDay("Каждый рабочий день"),
    Month("Каждый месяц");
    
    private String caption;

    public String getCaption() {
        return caption;
    }
    
    private RecordPeriodicity(String caption) {
        this.caption = caption;
    }
}
