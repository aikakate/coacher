package ru.bsu.coacher.shared;

public enum RecordValueType {
    Text("Текст"),
    Integer("Целое число"),
    Float("Вещественное число"),
    Enum("Перечисление");
    private final String caption;
    private RecordValueType(String caption) {
        this.caption = caption;
    }
    public String getCaption() {
        return caption;
    }
}
