package ru.bsu.coacher.shared;

public enum SportDegree {
    None("Без квалификации"),
    Third("Третий разряд"),
    Second("Второй разряд"),
    First("Первый разряд"),
    MasterCandidate("Кандидат в мастера спорта"),
    Master("Мастер спорта"),
    InternationalMaster("Мастер спорта международного класса");
    
    private final String caption;

    public String getCaption() {
        return caption;
    }
    
    private SportDegree(String caption) {
        this.caption = caption;
    }
}
