package ru.bsu.coacher.web;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import org.apache.shiro.SecurityUtils;
import ru.bsu.coacher.eb.AccountManager;
import ru.bsu.coacher.entities.Account;
import ru.bsu.coacher.shared.RecordSourceType;

@Named @RequestScoped
public class AccountBean {
    @EJB
    private AccountManager accMgr;
    
    public boolean canEdit(RecordSourceType type) {
        Account account = getAccount();
        switch (type) {
            case Athlete: return account.hasRole("athlete");
            case Coach: return account.hasRole("coach");
            case Medic: return account.hasRole("medic");
            case Psychologist: return account.hasRole("psychologist");
        }
        return false;
    }
    
    public Account getAccount() {
        return (Account)
            SecurityUtils.getSubject().getSession().getAttribute("account");
    }
    
    public <T extends Account> T getTypedAccount() {
        return (T)getAccount();
    }
    
    public Account getActualAccount() {
        return accMgr.find(getAccount().getId());
    }
    
    public <T extends Account> T getTypedActualAccount() {
        return (T)getActualAccount();
    }
}