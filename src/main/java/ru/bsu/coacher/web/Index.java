package ru.bsu.coacher.web;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import ru.bsu.coacher.entities.records.RecordValue;
import ru.bsu.coacher.shared.DefaultAccountData;

@Named
@RequestScoped
public class Index {
    private final DefaultAccountData data = new DefaultAccountData();
    
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    @PersistenceContext
    private EntityManager eman;
    
    public void test() throws Exception {
        List<RecordValue> results = eman.createQuery(
        "select v from RecordValue v where v.day = :date", RecordValue.class)
        .setParameter("date", new Date(date.getYear(), date.getMonth(), date.getDate(), 0, 0), TemporalType.TIMESTAMP)
        .getResultList();
    }

    public DefaultAccountData getData() {
        return data;
    }
}