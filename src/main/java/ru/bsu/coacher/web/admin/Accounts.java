package ru.bsu.coacher.web.admin;

import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import ru.bsu.coacher.eb.AccountManager;
import ru.bsu.coacher.entities.Account;

@Named
@RequestScoped
public class Accounts {
    @EJB
    private AccountManager accMgr;
    
    public List<Account> getAccounts() {
        return accMgr.findAll();
    }
    
    public void deleteAccount(Long uid, String outcome) {
        accMgr.deleteAccount(uid);
        FacesContext context = FacesContext.getCurrentInstance();
        if (outcome == null) {
            context.addMessage(
                null,
                new FacesMessage("Пользователь удален"));
        } else {
            try {
                context.getExternalContext().redirect(outcome);
            }
            catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }
}
