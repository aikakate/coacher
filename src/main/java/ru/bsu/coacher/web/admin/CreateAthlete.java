package ru.bsu.coacher.web.admin;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import ru.bsu.coacher.eb.AccountManager;
import ru.bsu.coacher.eb.AthleteManager;
import ru.bsu.coacher.shared.DefaultAthleteData;
import ru.bsu.coacher.shared.DefaultFullAccountData;

@Named
@RequestScoped
public class CreateAthlete {
    
    private final DefaultFullAccountData accData = new DefaultFullAccountData();
    private final DefaultAthleteData athData = new DefaultAthleteData();
    
    @EJB
    private AthleteManager athleteMgr;
    
    @EJB
    private AccountManager accMgr;

    public DefaultFullAccountData getAccData() {
        return accData;
    }

    public DefaultAthleteData getAthData() {
        return athData;
    }
    
    public String create() {
        long id = athleteMgr.createAthlete(
            accData.getEmail(),
            accData.getPassword(),
            true,
            athData).getId();
        accMgr.updateAccountData(id, accData);
        return "accounts.xhtml?faces-redirect=true";
    }
}
