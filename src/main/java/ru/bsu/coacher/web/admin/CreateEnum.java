package ru.bsu.coacher.web.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import ru.bsu.coacher.eb.EnumTypeManager;

@Named
@ViewScoped
public class CreateEnum implements Serializable {
    private final EnumModel model = new EnumModel();
    
    @EJB
    private EnumTypeManager etMgr;

    public EnumModel getModel() {
        return model;
    }
    
    public String create() {
        final List<String> values = new ArrayList<>();
        for (EnumValueModel evm : model.getValues()) {
            values.add(evm.getName());
        }
        etMgr.create(model.getName(), values, null);
        return "enums.xhtml?faces-redirect=true";
    }
}
