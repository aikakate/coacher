package ru.bsu.coacher.web.admin;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import ru.bsu.coacher.eb.AccountManager;
import ru.bsu.coacher.eb.PsychologistManager;
import ru.bsu.coacher.shared.DefaultFullAccountData;
import ru.bsu.coacher.shared.DefaultSpecialistData;

@Named
@RequestScoped
public class CreatePsychologist {
        
    private final DefaultFullAccountData accData = new DefaultFullAccountData();
    private final DefaultSpecialistData specData = new DefaultSpecialistData();
    
    @EJB
    private AccountManager accMgr;
    @EJB
    private PsychologistManager psyMgr;

    public DefaultFullAccountData getAccData() {
        return accData;
    }

    public DefaultSpecialistData getSpecData() {
        return specData;
    }
    
    public String create() {
        long id = psyMgr.createPsychologist(
            accData.getEmail(),
            accData.getPassword(),
            specData).getId();
        accMgr.updateAccountData(id, accData);
        return "accounts.xhtml?faces-redirect=true";
    }
}
