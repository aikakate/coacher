package ru.bsu.coacher.web.admin;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import ru.bsu.coacher.eb.EnumTypeManager;
import ru.bsu.coacher.eb.RecordManager;
import ru.bsu.coacher.entities.EnumType;

@Named
@ViewScoped
public class CreateRecordGroup implements Serializable {
    
    private final RecordGroupModel model = new RecordGroupModel();
    
    @EJB
    private RecordManager recMgr;
    @EJB
    private EnumTypeManager enumMgr;
    
    public String create() {
        long recGroupId = recMgr.addRecordGroup(
            model.getName(),
            model.getSourceType(),
            model.getPeriodicity(),
            model.getComment());
        for (RecordModel recMdl : model.getRecords()) {
            Long enumTypeId = recMdl.getEnumTypeId();
            EnumType enumType = null;
            if (enumTypeId != null) {
                enumType = enumMgr.find(recMdl.getEnumTypeId());
            }
            recMgr.addRecord(
                recGroupId, 
                recMdl.getName(), 
                recMdl.getValueType(),
                enumType,
                recMdl.getComment());
        }
        return "record_groups.xhtml?faces-redirect=true";
    }

    public RecordGroupModel getModel() {
        return model;
    }
}
