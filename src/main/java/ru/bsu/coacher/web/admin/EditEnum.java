package ru.bsu.coacher.web.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import ru.bsu.coacher.eb.EnumTypeManager;
import ru.bsu.coacher.eb.EnumValueManager;
import ru.bsu.coacher.entities.EnumType;
import ru.bsu.coacher.entities.EnumValue;

@Named
@ViewScoped
public class EditEnum implements Serializable {
    private final EnumModel model = new EnumModel();
    private long id;
    private boolean initialized;
    
    @EJB
    private EnumTypeManager etMgr;
    
    @EJB
    private EnumValueManager evMgr;

    public EnumModel getModel() {
        return model;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public void init() {
        if (initialized)
            return;
        
        EnumType enumType = etMgr.find(id);
        model.setId(enumType.getId());
        model.setName(enumType.getName());
        for (EnumValue enumValue : enumType.getValues()) {
            EnumValueModel valModel = new EnumValueModel();
            valModel.setId(enumValue.getId());
            valModel.setName(enumValue.getName());
            valModel.setDescription(enumValue.getDescription());
            model.getValues().add(valModel);
        }
        
        initialized = true;
    }
    
    public String update() {
        // add / update
        List<EnumValue> oldValues = etMgr.find(id).getValues();
        Set<Long> modelIds = new HashSet<>();
        for (EnumValueModel evModel : model.getValues()) {
            Long evId = evModel.getId();
            if (evId == null) {
                evMgr.create(id, evModel.getName(), evModel.getDescription());
            } else {
                evMgr.update(evId, evModel.getName(), evModel.getDescription());
                modelIds.add(evId);
            }
        }
        // remove
        for (EnumValue ev : oldValues) {
            if (!modelIds.contains(ev.getId())) {
                evMgr.delete(ev.getId());
            }
        }
        return "enums.xhtml?faces-redirect=true";
    }
}
