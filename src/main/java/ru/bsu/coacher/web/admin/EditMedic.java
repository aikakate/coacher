package ru.bsu.coacher.web.admin;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import ru.bsu.coacher.eb.AccountManager;
import ru.bsu.coacher.eb.MedicManager;
import ru.bsu.coacher.entities.Medic;
import ru.bsu.coacher.shared.DataMapper;
import ru.bsu.coacher.shared.DefaultAccountData;
import ru.bsu.coacher.shared.DefaultSpecialistData;

@Named
@ViewScoped
public class EditMedic implements Serializable {
    private final DefaultAccountData accData = new DefaultAccountData();
    private final DefaultSpecialistData specData = new DefaultSpecialistData();

    public DefaultAccountData getAccData() {
        return accData;
    }

    public DefaultSpecialistData getSpecData() {
        return specData;
    }
    
    @EJB
    private AccountManager accMgr;
    @EJB
    private MedicManager medMgr;
    
    private long medicId;

    public long getMedicId() {
        return medicId;
    }

    public void setMedicId(long medicId) {
        this.medicId = medicId;
        Medic medic = medMgr.findMedic(medicId);
        DataMapper.map(medic, accData);
        DataMapper.map(medic, specData);
    }
    
    public void updateData() {
        accMgr.updateAccountData(medicId, accData);
        medMgr.updateMedicData(medicId, specData);
        FacesContext.getCurrentInstance().addMessage(
                null, new FacesMessage("Изменения применены"));
    }
}
