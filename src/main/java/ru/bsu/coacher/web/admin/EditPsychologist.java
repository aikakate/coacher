package ru.bsu.coacher.web.admin;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import ru.bsu.coacher.eb.AccountManager;
import ru.bsu.coacher.eb.PsychologistManager;
import ru.bsu.coacher.entities.Psychologist;
import ru.bsu.coacher.shared.DataMapper;
import ru.bsu.coacher.shared.DefaultAccountData;
import ru.bsu.coacher.shared.DefaultSpecialistData;

@Named
@ViewScoped
public class EditPsychologist implements Serializable {

    private long psychologistId;
    private final DefaultAccountData accData = new DefaultAccountData();
    private final DefaultSpecialistData specData = new DefaultSpecialistData();

    @EJB
    private AccountManager accMgr;
    @EJB
    private PsychologistManager psyMgr;

    public DefaultAccountData getAccData() {
        return accData;
    }

    public DefaultSpecialistData getSpecData() {
        return specData;
    }

    public long getPsychologistId() {
        return psychologistId;
    }

    public void setPsychologistId(long psychoId) {
        this.psychologistId = psychoId;
        Psychologist psych = psyMgr.findPsychologist(psychoId);
        DataMapper.map(psych, accData);
        DataMapper.map(psych, specData);
    }

    public void updateData() {
        accMgr.updateAccountData(psychologistId, accData);
        psyMgr.updatePsychologistData(psychologistId, specData);
        FacesContext.getCurrentInstance().addMessage(
                null, new FacesMessage("Изменения применены"));
    }
}
