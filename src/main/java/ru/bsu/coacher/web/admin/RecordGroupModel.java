
package ru.bsu.coacher.web.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import ru.bsu.coacher.shared.RecordPeriodicity;
import ru.bsu.coacher.shared.RecordSourceType;
import ru.bsu.coacher.shared.RecordValueType;

public class RecordGroupModel implements Serializable {
    private Long id;
    private final List<RecordModel> records = new ArrayList<>();
    private String name;
    private String comment;
    private RecordPeriodicity periodicity;
    private RecordSourceType sourceType;

    public List<RecordModel> getRecords() {
        return records;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public RecordPeriodicity getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(RecordPeriodicity periodicity) {
        this.periodicity = periodicity;
    }

    public RecordSourceType getSourceType() {
        return sourceType;
    }

    public void setSourceType(RecordSourceType sourceType) {
        this.sourceType = sourceType;
    }
    
    public RecordPeriodicity[] getAllPeriodicities() {
        return RecordPeriodicity.values();
    }
    
    public RecordSourceType[] getAllSourceTypes() {
        return RecordSourceType.values();
    }
    
    public RecordValueType[] getAllValueTypes() {
        return RecordValueType.values();
    }
    
    public void addRecord(){
        records.add(new RecordModel());
    }
    
    public void removeRecord(RecordModel model) {
        records.remove(model);
    }
}
