package ru.bsu.coacher.web.cabinet;

import java.io.Serializable;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import ru.bsu.coacher.eb.RecordManager;
import ru.bsu.coacher.entities.records.RecordGroup;
import ru.bsu.coacher.shared.RecordSourceType;

@Named
@ViewScoped
public class CabinetRecordGroups implements Serializable {
    @EJB
    private RecordManager recMgr;
    
    private int getOrderWeight(RecordSourceType rst) {
        switch (rst) {
            case Athlete: return 0;
            case Coach: return 1;
            case Medic: return 2;
            case Psychologist: return 3;
        }
        return 42;
    }
    
    public List<RecordGroup> getGroups() {
        return recMgr.findAllRecordGroups().stream().sorted(
                (a, b) -> Integer.compare(
                        getOrderWeight(a.getSourceType()),
                        getOrderWeight(b.getSourceType())))
                .collect(Collectors.toList());
    }
}
