package ru.bsu.coacher.web.cabinet;

import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import ru.bsu.coacher.entities.Account;
import ru.bsu.coacher.entities.Athlete;
import ru.bsu.coacher.entities.Coach;
import ru.bsu.coacher.entities.Master;
import ru.bsu.coacher.entities.Medic;
import ru.bsu.coacher.entities.Psychologist;
import ru.bsu.coacher.web.AccountBean;

@RequestScoped
@Named
public class Masters {
    @Inject
    private AccountBean accBean;
    
    private Integer getOrderWeight(Account account) {
        if (account instanceof Coach)
            return 0;
        return account.getId().intValue();
    }
    
    public String getMasterTypeCaption(Account account) {
        if (account instanceof Coach)
            return "Тренер";
        else if (account instanceof Medic)
            return "Врач";
        else if (account instanceof Psychologist)
            return "Психолог";
        return "?";
    }
    
    public List<Master> getMasters() {
        Athlete athlete = (Athlete)accBean.getActualAccount();
        return athlete.getMasters()
                .stream()
                .sorted((a, b) -> Integer.compare(
                        getOrderWeight(a), getOrderWeight(b)))
                .collect(Collectors.toList());
    }
}
