package ru.bsu.coacher.web.cabinet;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import ru.bsu.coacher.eb.AccountManager;
import ru.bsu.coacher.eb.AthleteManager;
import ru.bsu.coacher.eb.CoachManager;
import ru.bsu.coacher.eb.MedicManager;
import ru.bsu.coacher.eb.PsychologistManager;
import ru.bsu.coacher.entities.Account;
import ru.bsu.coacher.entities.Athlete;
import ru.bsu.coacher.entities.Coach;
import ru.bsu.coacher.entities.Medic;
import ru.bsu.coacher.entities.Psychologist;
import ru.bsu.coacher.entities.Specialist;
import ru.bsu.coacher.shared.DataMapper;
import ru.bsu.coacher.shared.DefaultAccountData;
import ru.bsu.coacher.shared.DefaultAthleteData;
import ru.bsu.coacher.shared.DefaultCoachData;
import ru.bsu.coacher.shared.DefaultSpecialistData;
import ru.bsu.coacher.web.AccountBean;

@Named
@ViewScoped
public class MyProfileInfo implements Serializable {

    private final DefaultAccountData accountData = new DefaultAccountData();
    private final DefaultAthleteData athleteData = new DefaultAthleteData();
    private final DefaultSpecialistData specData = new DefaultSpecialistData();
    private final DefaultCoachData coachData = new DefaultCoachData();

    @EJB
    private AccountManager accMgr;
    @EJB
    private AthleteManager athMgr;
    @EJB
    private CoachManager coachMgr;
    @EJB
    private MedicManager medMgr;
    @EJB
    private PsychologistManager psyMgr;

    @Inject
    private AccountBean accBean;

    public DefaultAccountData getAccountData() {
        return accountData;
    }

    public DefaultAthleteData getAthleteData() {
        return athleteData;
    }

    public DefaultSpecialistData getSpecData() {
        return specData;
    }

    public DefaultCoachData getCoachData() {
        return coachData;
    }

    @PostConstruct
    public void init() {
        Account account = accBean.getActualAccount();
        DataMapper.map(account, accountData);
        if (account instanceof Athlete) {
            Athlete athlete = (Athlete) account;
            DataMapper.map(athlete, athleteData);
        } else if (account instanceof Coach) {
            Coach coach = (Coach) account;
            DataMapper.map(coach, coachData);
        } else if (account instanceof Specialist) {
            Specialist specialist = (Specialist) account;
            DataMapper.map(specialist, specData);
        }
    }

    public void update() {
        Account account = accBean.getAccount();

        accMgr.updateAccountData(account.getId(), accountData);
        if (account instanceof Athlete) {
            athMgr.updateAthleteData(account.getId(), athleteData);
        }
        else if (account instanceof Coach) {
            coachMgr.updateCoachData(account.getId(), coachData);
        }
        else if (account instanceof Medic) {
            medMgr.updateMedicData(account.getId(), specData);
        }
        else if (account instanceof Psychologist) {
            psyMgr.updatePsychologistData(account.getId(), specData);
        }
    }

    public String getFoo() {
        return "";
    }
}
