package ru.bsu.coacher.web.cabinet;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import ru.bsu.coacher.eb.AthleteManager;
import ru.bsu.coacher.entities.AthleteRequest;
import ru.bsu.coacher.web.AccountBean;

@Named
@RequestScoped
public class PendingRequests {
    @EJB
    private AthleteManager athMgr;
    
    @Inject
    private AccountBean accBean;
    
    public AthleteRequest getRequest() {
        return athMgr.findAthleteRequest(accBean.getAccount().getId());
    }
}