package ru.bsu.coacher.web.cabinet;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import ru.bsu.coacher.eb.AccountManager;
import ru.bsu.coacher.entities.Account;
import ru.bsu.coacher.entities.Athlete;
import ru.bsu.coacher.entities.Coach;
import ru.bsu.coacher.entities.Specialist;
import ru.bsu.coacher.shared.AccountData;
import ru.bsu.coacher.shared.AthleteData;
import ru.bsu.coacher.shared.CoachData;
import ru.bsu.coacher.shared.DataMapper;
import ru.bsu.coacher.shared.DefaultAccountData;
import ru.bsu.coacher.shared.DefaultAthleteData;
import ru.bsu.coacher.shared.DefaultCoachData;
import ru.bsu.coacher.shared.DefaultSpecialistData;
import ru.bsu.coacher.shared.SpecialistData;

@Named
@RequestScoped
public class ProfileInfo {
    private Long id;
    private final AccountData accData = new DefaultAccountData();
    private AthleteData athData;
    private CoachData coachData;
    private SpecialistData specData;
    
    @EJB
    private AccountManager accMgr;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
        Account account = accMgr.find(id);
        DataMapper.map(account, accData);
        if (account instanceof Athlete) {
            athData = new DefaultAthleteData();
            DataMapper.map((Athlete)account, athData);
        } else if (account instanceof Coach) {
            coachData = new DefaultCoachData();
            DataMapper.map((Coach)account, coachData);
        } else if (account instanceof Specialist) {
            specData = new DefaultSpecialistData();
            DataMapper.map((Specialist)account, specData);
        }
    }

    public AccountData getAccData() {
        return accData;
    }

    public AthleteData getAthData() {
        return athData;
    }

    public CoachData getCoachData() {
        return coachData;
    }

    public SpecialistData getSpecData() {
        return specData;
    }
}
