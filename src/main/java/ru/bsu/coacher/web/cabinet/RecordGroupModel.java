package ru.bsu.coacher.web.cabinet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import ru.bsu.coacher.shared.RecordPeriodicity;
import ru.bsu.coacher.shared.RecordSourceType;

public class RecordGroupModel implements Serializable {
    private final List<RecordValueModel> valueModels = new ArrayList<>();
    private long id;
    private String name;
    private RecordPeriodicity periodicity;
    private RecordSourceType sourceType;
    
    public List<RecordValueModel> getValueModels() {
        return valueModels;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RecordPeriodicity getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(RecordPeriodicity periodicity) {
        this.periodicity = periodicity;
    }

    public RecordSourceType getSourceType() {
        return sourceType;
    }

    public void setSourceType(RecordSourceType sourceType) {
        this.sourceType = sourceType;
    }
}
