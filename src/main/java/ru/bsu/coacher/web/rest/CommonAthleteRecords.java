package ru.bsu.coacher.web.rest;

import java.util.Date;

public class CommonAthleteRecords {
    private Date date;
    
    private Integer morningLyingPulseRate;
    private Integer morningStandingPulseRate;
    private Integer diastolicBloodPressure;
    private Integer systolicBloodPressure;
    private Integer sleepValueCode;
    private Integer complainsValueCode;
    private Integer moodValueCode;
    private Integer feelingValueCode;
    private Integer operabilityValueCode;
    private Integer appetiteValueCode;
    private Integer bowelFunctionValueCode;
    
    private Integer trainingMoodValueCode;
    private Float beforeTrainingWeight;
    private Integer beforeTrainingPulseRate;
    private Integer trainingRegimeValueCode;
    private String trainingPlan;
    
    private Integer afterTrainingPulseRate;
    private Float afterTrainingWeight;
    private Integer sweatingCodeId;
    private Integer loadHandlingCodeId;
    private String sportFood;
    private String drugs;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getMorningLyingPulseRate() {
        return morningLyingPulseRate;
    }

    public void setMorningLyingPulseRate(Integer morningLyingPulseRate) {
        this.morningLyingPulseRate = morningLyingPulseRate;
    }

    public Integer getMorningStandingPulseRate() {
        return morningStandingPulseRate;
    }

    public void setMorningStandingPulseRate(Integer morningStandingPulseRate) {
        this.morningStandingPulseRate = morningStandingPulseRate;
    }

    public Integer getDiastolicBloodPressure() {
        return diastolicBloodPressure;
    }

    public void setDiastolicBloodPressure(Integer diastolicBloodPressure) {
        this.diastolicBloodPressure = diastolicBloodPressure;
    }

    public Integer getSystolicBloodPressure() {
        return systolicBloodPressure;
    }

    public void setSystolicBloodPressure(Integer systolicBloodPressure) {
        this.systolicBloodPressure = systolicBloodPressure;
    }

    public Integer getSleepValueCode() {
        return sleepValueCode;
    }

    public void setSleepValueCode(Integer sleepValueCode) {
        this.sleepValueCode = sleepValueCode;
    }

    public Integer getComplainsValueCode() {
        return complainsValueCode;
    }

    public void setComplainsValueCode(Integer complainsValueCode) {
        this.complainsValueCode = complainsValueCode;
    }

    public Integer getMoodValueCode() {
        return moodValueCode;
    }

    public void setMoodValueCode(Integer moodValueCode) {
        this.moodValueCode = moodValueCode;
    }

    public Integer getFeelingValueCode() {
        return feelingValueCode;
    }

    public void setFeelingValueCode(Integer feelingValueCode) {
        this.feelingValueCode = feelingValueCode;
    }

    public Integer getOperabilityValueCode() {
        return operabilityValueCode;
    }

    public void setOperabilityValueCode(Integer operabilityValueCode) {
        this.operabilityValueCode = operabilityValueCode;
    }

    public Integer getAppetiteValueCode() {
        return appetiteValueCode;
    }

    public void setAppetiteValueCode(Integer appetiteValueCode) {
        this.appetiteValueCode = appetiteValueCode;
    }

    public Integer getBowelFunctionValueCode() {
        return bowelFunctionValueCode;
    }

    public void setBowelFunctionValueCode(Integer bowelFunctionValueCode) {
        this.bowelFunctionValueCode = bowelFunctionValueCode;
    }

    public Integer getTrainingMoodValueCode() {
        return trainingMoodValueCode;
    }

    public void setTrainingMoodValueCode(Integer trainingMoodValueCode) {
        this.trainingMoodValueCode = trainingMoodValueCode;
    }

    public Float getBeforeTrainingWeight() {
        return beforeTrainingWeight;
    }

    public void setBeforeTrainingWeight(Float beforeTrainingWeight) {
        this.beforeTrainingWeight = beforeTrainingWeight;
    }

    public Integer getBeforeTrainingPulseRate() {
        return beforeTrainingPulseRate;
    }

    public void setBeforeTrainingPulseRate(Integer beforeTrainingPulseRate) {
        this.beforeTrainingPulseRate = beforeTrainingPulseRate;
    }

    public Integer getTrainingRegimeValueCode() {
        return trainingRegimeValueCode;
    }

    public void setTrainingRegimeValueCode(Integer trainingRegimeValueCode) {
        this.trainingRegimeValueCode = trainingRegimeValueCode;
    }

    public String getTrainingPlan() {
        return trainingPlan;
    }

    public void setTrainingPlan(String trainingPlan) {
        this.trainingPlan = trainingPlan;
    }

    public Integer getAfterTrainingPulseRate() {
        return afterTrainingPulseRate;
    }

    public void setAfterTrainingPulseRate(Integer afterTrainingPulseRate) {
        this.afterTrainingPulseRate = afterTrainingPulseRate;
    }

    public Float getAfterTrainingWeight() {
        return afterTrainingWeight;
    }

    public void setAfterTrainingWeight(Float afterTrainingWeight) {
        this.afterTrainingWeight = afterTrainingWeight;
    }

    public Integer getSweatingCodeId() {
        return sweatingCodeId;
    }

    public void setSweatingCodeId(Integer sweatingCodeId) {
        this.sweatingCodeId = sweatingCodeId;
    }

    public Integer getLoadHandlingCodeId() {
        return loadHandlingCodeId;
    }

    public void setLoadHandlingCodeId(Integer loadHandlingCodeId) {
        this.loadHandlingCodeId = loadHandlingCodeId;
    }

    public String getSportFood() {
        return sportFood;
    }

    public void setSportFood(String sportFood) {
        this.sportFood = sportFood;
    }

    public String getDrugs() {
        return drugs;
    }

    public void setDrugs(String drugs) {
        this.drugs = drugs;
    }
}
