package ru.bsu.coacher.web.rest;

import ru.bsu.coacher.shared.DefaultAccountData;
import ru.bsu.coacher.shared.DefaultAthleteData;

public class GetAthleteDataResponse extends BasicResponse  {
    private DefaultAthleteData athleteData;
    private DefaultAccountData accountData;

    public DefaultAthleteData getAthleteData() {
        return athleteData;
    }

    public void setAthleteData(DefaultAthleteData athleteData) {
        this.athleteData = athleteData;
    }

    public DefaultAccountData getAccountData() {
        return accountData;
    }

    public void setAccountData(DefaultAccountData accountData) {
        this.accountData = accountData;
    }
}
