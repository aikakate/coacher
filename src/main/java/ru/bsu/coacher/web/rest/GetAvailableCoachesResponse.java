package ru.bsu.coacher.web.rest;

import java.util.List;

public class GetAvailableCoachesResponse extends BasicResponse {
    private List<AccountModel> coaches;

    public List<AccountModel> getCoaches() {
        return coaches;
    }

    public void setCoaches(List<AccountModel> coaches) {
        this.coaches = coaches;
    }
}