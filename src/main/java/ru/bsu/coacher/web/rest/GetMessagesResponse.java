package ru.bsu.coacher.web.rest;

import java.util.Collection;

public class GetMessagesResponse extends BasicResponse {
    private Collection<MessageModel> messages;

    public Collection<MessageModel> getMessages() {
        return messages;
    }

    public void setMessages(Collection<MessageModel> messages) {
        this.messages = messages;
    }
}
