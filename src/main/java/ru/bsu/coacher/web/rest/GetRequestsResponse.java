package ru.bsu.coacher.web.rest;

import java.io.Serializable;
import java.util.List;

public class GetRequestsResponse extends BasicResponse implements Serializable {
    private List<RequestModel> requests;

    public List<RequestModel> getRequests() {
        return requests;
    }

    public void setRequests(List<RequestModel> requests) {
        this.requests = requests;
    }
    
}