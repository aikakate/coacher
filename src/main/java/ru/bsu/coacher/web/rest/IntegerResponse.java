
package ru.bsu.coacher.web.rest;

public class IntegerResponse extends BasicResponse {
    private Integer result;

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }
    
}
