package ru.bsu.coacher.web.rest;

public class LoginResponse extends BasicResponse {
    public String mainRole;

    public String getMainRole() {
        return mainRole;
    }

    public void setMainRole(String mainRole) {
        this.mainRole = mainRole;
    }
}