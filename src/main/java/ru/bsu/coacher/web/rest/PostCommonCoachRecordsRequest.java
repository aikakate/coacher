package ru.bsu.coacher.web.rest;

import java.util.Date;

public class PostCommonCoachRecordsRequest {
    private CommonCoachRecords records;
    private Long athleteId;
    private Date day;

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public Long getAthleteId() {
        return athleteId;
    }

    public void setAthleteId(Long athleteId) {
        this.athleteId = athleteId;
    }

    public CommonCoachRecords getRecords() {
        return records;
    }

    public void setRecords(CommonCoachRecords records) {
        this.records = records;
    }
}
