package ru.bsu.coacher.web.rest;

import java.io.Serializable;

public class RequestModel implements Serializable {
    private Long id;
    private AccountModel account;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AccountModel getAccount() {
        return account;
    }

    public void setAccount(AccountModel account) {
        this.account = account;
    }
    
    public RequestModel() {
        
    }
    
    public RequestModel(Long id, AccountModel account) {
        this.id = id;
        this.account = account;
    }
}