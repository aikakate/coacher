package ru.bsu.coacher.web.rest;

public class SendMessageParameters {
    private Long receiverId;
    private String text;

    public Long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Long receiverId) {
        this.receiverId = receiverId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
